﻿using System;
using System.Collections.Generic;
using System.Text;
using PWalnik.SDManager.DataModel.Models;
using PWalnik.SDManager.DataModel.ViewModels;
using System.Threading.Tasks;

namespace PWalnik.SDManager.Interfaces
{
    public interface IReportsService
    {
        Task<List<Order>> GetLastOrdersAsync(string searchString);
    }
}
