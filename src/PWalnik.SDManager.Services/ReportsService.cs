﻿using System;
using System.Collections.Generic;
using System.Text;
using PWalnik.SDManager.Interfaces;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PWalnik.SDManager.DataModel.Data;
using PWalnik.SDManager.DataModel.Models;
using PWalnik.SDManager.DataModel.ViewModels;
using System.Linq;

namespace PWalnik.SDManager.Services
{
    public class ReportsService : IReportsService
    {
        private readonly ApplicationDbContext _context;

        public ReportsService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<Order>> GetLastOrdersAsync(string searchString)
        {
            var orders = new List<Order>();
            if (!String.IsNullOrEmpty(searchString))
                orders = await _context.Orders.Where(o => o.Customer.CompanyName == searchString || o.Employee.LastName == searchString).OrderByDescending(o => o.OrderDate).Take(10).ToListAsync();
            return orders;
        }
    }
}
