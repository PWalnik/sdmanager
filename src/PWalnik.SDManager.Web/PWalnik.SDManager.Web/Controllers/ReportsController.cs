using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PWalnik.SDManager.DataModel.Models;
using PWalnik.SDManager.Interfaces;
using PWalnik.SDManager.Services.Requirements;

namespace PWalnik.SDManager.Web.Controllers
{
    [Authorize(Roles = "SalesRepresentative, Manager")]
    public class ReportsController : Controller
    {
        private readonly IReportsService _reportsService;
        private readonly IAuthorizationService _authorizationService;

        public ReportsController(IReportsService reportsService, IAuthorizationService authorizationService)
        {
            _reportsService = reportsService;
            _authorizationService = authorizationService;
        }

        [HttpGet]
        public async Task<IActionResult> Index(string searchString)
        {
            return View(await _reportsService.GetLastOrdersAsync(searchString));
        }


    }
}